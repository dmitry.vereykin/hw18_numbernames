/**
 * Created by Dmitry on 7/2/2015.
 */

import java.util.Scanner;

 public class NumberNamesDebug {

    private static final String[] tensKor = { "", "십"};

    private static final String[] numKor = { "", "일", "이", "삼", "사", "오", "육", "칠", "팔", "구", "십"};

    private static final String[] specialKor = { "", "천", "만"};

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        int number = 0;
        String numberFinal = "";

        do {
            System.out.print("Please enter a number (0 to 999,999,999) or -1 to quit: ");
            number = keyboard.nextInt();
                if (number == -1) break;
            numberFinal = convertEng(number);
            System.out.println(number + "  =  " + numberFinal);
            System.out.println("---------------------------------------------------");
        } while (number != -1);

        keyboard.close();
    }

    public static String convertLessThanThousandEng(int number) {
        String current;

        if (number % 100 < 10) {
            current = numKor[number % 100];
            number /= 100;
        }
        else {
            current = numKor[number % 10];
            number /= 10;

            current = numKor[number % 10] + tensKor[1] + current;
            number /= 10;
        }
        if (number == 0) return current;
        return numKor[number] + "백" + current;
    }

    public static String convertEng(int number) {
        if (number == 0) {
            return " 영";
        }

        String current = "";
        int place = 0;

        do {
            int n = number % 1000;
            if (n != 0){
                String s = convertLessThanThousandEng(n);
                current = s + specialKor[place] + current;
            }
            place++;
            number /= 1000;
        } while (number > 0);

        return (current).trim();
    }
}
